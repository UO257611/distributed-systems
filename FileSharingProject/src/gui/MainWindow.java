package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.FileChooserUI;

import org.jvnet.substance.skin.SubstanceBusinessBlackSteelLookAndFeel;

import interfaces.FileMonitor;
import javafx.stage.FileChooser;
import logic.Monitor.FileMonitorImp;
import logic.Monitor.LocalFolderLogic;
import logic.ObserverObservable.FolderObservable;
import logic.ObserverObservable.LocalObserver;
import logic.ObserverObservable.SharedObserver;
import logic.Persistance.FileManager;
import logic.PoolAndThreads.DownloadUploadWorker;
import logic.PoolAndThreads.WorkersPool;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JInternalFrame;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.JTextArea;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.awt.event.ActionEvent;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JMenuItem;
import javax.swing.JProgressBar;

/**
 * 
 * @author Eduardo Lamas Suarez
 * 
 * This clase is for the gui
 *
 */
public class MainWindow extends JFrame {
	private LocalObserver localObserver;
	private SharedObserver sharedObserver;
	private FolderObservable folderOsbservable;
	private final static Byte ERROR_WHILE_READING_BYTES = -1;
	private static MainWindow MAIN_WINDOW;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenu mnEdit;
	private JMenu mnHelp;
	private JPanel pnButtonsAndInfo;
	private JPanel pnLists;
	private JScrollPane spShared;
	private JList lsShared;
	private JScrollPane spLocal;
	private JList lsLocal;
	private JPanel pnSharedTitle;
	private JLabel lblShared;
	private JPanel pnLocalTitle;
	private JLabel lblLocal;
	private JPanel pnListButtons;
	private JPanel pnEmpty1;
	private JPanel pnProgress;
	private JPanel pnListControl;
	private JButton btnSearchLocalFolder;
	private FileMonitor fileMonitor;
	private DefaultListModel<String> sharedListModel;
	private DefaultListModel<String> localListModel;
	private JPanel pnButtons;
	private JPanel pnInfo;
	private JButton btnPlay;
	private JButton btnStop;
	private JLabel lblPlayign;
	private JLabel lblSongName;
	private static String PATH_LOCAL_FOLDER = null;
	private JButton btnDownlad;
	private Timer timer;
	private LocalFolderLogic localManager;
	private FileManager fileManager;
	private WorkersPool workersPool;
	
	private JButton btnUpload;
	private JMenuItem miExit;
	private JLabel lblDownloadProgress;
	private JProgressBar progressBar;
	


	private ActionListener checkingSharedList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//Set up the look and fell to make the gui prettier
					JFrame.setDefaultLookAndFeelDecorated(true);
					SubstanceBusinessBlackSteelLookAndFeel.setSkin("org.jvnet.substance.skin.EmeraldDuskSkin");
					
					MainWindow frame = new MainWindow();
					MAIN_WINDOW = frame;
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle("Sharing Music 3000");
		//-------------------------------------------
		//Set up some variable for threading, persistence and monitoring the folders
		workersPool = WorkersPool.getInstance(this);
		fileManager = new FileManager();
		localManager = LocalFolderLogic.getInstance();
		fileMonitor = FileMonitorImp.getInstance();
		//-----------------------------------------------
		// Set up of the observers and observables
		localObserver = new LocalObserver(this);
		sharedObserver = new SharedObserver(this);
		folderOsbservable= new FolderObservable(fileMonitor, localManager);
		folderOsbservable.addObserver(localObserver);
		folderOsbservable.addObserver(sharedObserver);
		//----------------------JFrame set up-------------------------
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 794, 552);
		setJMenuBar(getMenuBar_1());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(getPnButtonsAndInfo(), BorderLayout.SOUTH);
		contentPane.add(getPnLists(), BorderLayout.CENTER);
		contentPane.add(getPnListButtons(), BorderLayout.EAST);
		//------------------------------------------------------
		//Check if it is stored in the additional file a path for the local folder
		checkIfExistLocalFolder();

		/**
		 * This is the action listener for the timer to be executing every 5 seconds
		 */
		checkingSharedList= new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	if(fileManager.isThereInfoFile()&& PATH_LOCAL_FOLDER != null)
	        		folderOsbservable.setChanged();
	        }
		};
		
		
		//THis will activate the timer that check if there was an error with the folder observable
		timer = new Timer(5000 ,checkingSharedList); 
		timer.setRepeats(true);
		timer.start();
	}

//----------------------------------------------------------------------------------------------------------------
//----------------------Here is going to start a code section not very important of -------------------------------
//---------------------------------basic components and normal getters------------------------------------------------
//----------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

	
	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnFile());
			menuBar.add(getMnEdit());
			menuBar.add(getMnHelp());
		}
		return menuBar;
	}
	private JMenu getMnFile() {
		if (mnFile == null) {
			mnFile = new JMenu("File");
			mnFile.add(getMiExit());
		}
		return mnFile;
	}
	private JMenu getMnEdit() {
		if (mnEdit == null) {
			mnEdit = new JMenu("Edit");
		}
		return mnEdit;
	}
	private JMenu getMnHelp() {
		if (mnHelp == null) {
			mnHelp = new JMenu("Help");
		}
		return mnHelp;
	}
	private JPanel getPnButtonsAndInfo() {
		if (pnButtonsAndInfo == null) {
			pnButtonsAndInfo = new JPanel();
			pnButtonsAndInfo.setLayout(new GridLayout(1, 1, 0, 0));
			pnButtonsAndInfo.add(getPnButtons());
			pnButtonsAndInfo.add(getPnInfo());
		}
		return pnButtonsAndInfo;
	}
	private JPanel getPnLists() {
		if (pnLists == null) {
			pnLists = new JPanel();
			pnLists.setLayout(new GridLayout(0, 2, 0, 0));
			pnLists.add(getSpShared());
			pnLists.add(getSpLocal());
		}
		return pnLists;
	}
	private JScrollPane getSpShared() {
		if (spShared == null) {
			spShared = new JScrollPane();
			spShared.setViewportView(getLsShared());
			spShared.setColumnHeaderView(getPnSharedTitle());
		}
		return spShared;
	}
	public JList getLsShared() {
		if (lsShared == null) {
			lsShared = new JList();
			lsShared.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) 
				{
					if(getLsLocal().isEnabled())
							getBtnDownlad().setEnabled(true);
					
				}
				
			});
			lsShared.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			lsShared.setModel(new DefaultListModel<String>());
			sharedObserver.update();
		}
		return lsShared;
	}
	
	private JMenuItem getMiExit() {
		if (miExit == null) {
			miExit = new JMenuItem("Exit");
			miExit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					MAIN_WINDOW.dispose();
				}
			});
		}
		return miExit;
	}
	
	

	
	
	
	private JScrollPane getSpLocal() {
		if (spLocal == null) {
			spLocal = new JScrollPane();
			spLocal.setViewportView(getLsLocal());
			spLocal.setColumnHeaderView(getPnLocalTitle());
		}
		return spLocal;
	}
	private JList getLsLocal() {
		if (lsLocal == null) {
			lsLocal = new JList();
			lsLocal.addListSelectionListener(new ListSelectionListener() {
				public void valueChanged(ListSelectionEvent e) 
				{
					getBtnUpload().setEnabled(true);
				}
			});
			lsLocal.setEnabled(false);
			setLocalListModel(new DefaultListModel<>());
			lsLocal.setModel(getLocalListModel());
		}
		return lsLocal;
	}
	private JPanel getPnSharedTitle() {
		if (pnSharedTitle == null) {
			pnSharedTitle = new JPanel();
			pnSharedTitle.add(getLblShared());
		}
		return pnSharedTitle;
	}
	
		
	public LocalFolderLogic getLocalManager() {
		return localManager;
	}
	
	public void setLocalManager(LocalFolderLogic localManager) {
		this.localManager = localManager;
	}
	
	public void setFileMonitor(FileMonitor fileMonitor) {
		this.fileMonitor = fileMonitor;
	}

	private JLabel getLblShared() {
		if (lblShared == null) {
			lblShared = new JLabel("Shared Folder");
		}
		return lblShared;
	}
	private JPanel getPnLocalTitle() {
		if (pnLocalTitle == null) {
			pnLocalTitle = new JPanel();
			pnLocalTitle.add(getLblLocal());
		}
		return pnLocalTitle;
	}
	private JLabel getLblLocal() {
		if (lblLocal == null) {
			lblLocal = new JLabel("Local Folder");
		}
		return lblLocal;
	}
	private JPanel getPnListButtons() {
		if (pnListButtons == null) {
			pnListButtons = new JPanel();
			pnListButtons.setLayout(new GridLayout(0, 1, 0, 0));
			pnListButtons.add(getPnEmpty1());
			pnListButtons.add(getPnListControl());
			pnListButtons.add(getPnProgress());
		}
		return pnListButtons;
	}
	private JPanel getPnEmpty1() {
		if (pnEmpty1 == null) {
			pnEmpty1 = new JPanel();
		}
		return pnEmpty1;
	}
	private JPanel getPnProgress() {
		if (pnProgress == null) {
			pnProgress = new JPanel();
			pnProgress.setLayout(new GridLayout(2, 0, 0, 0));
			pnProgress.add(getLblDownloadProgress());
			pnProgress.add(getProgressBar());
		}
		return pnProgress;
	}
	private JPanel getPnListControl() {
		if (pnListControl == null) {
			pnListControl = new JPanel();
			pnListControl.setLayout(new BoxLayout(pnListControl, BoxLayout.Y_AXIS));
			pnListControl.add(getBtnSearchLocalFolder());
			pnListControl.add(getBtnDownlad());
			pnListControl.add(getBtnUpload());
		}
		return pnListControl;
	}
	
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			pnButtons.add(getBtnPlay());
			pnButtons.add(getBtnStop());
		}
		return pnButtons;
	}
	private JPanel getPnInfo() {
		if (pnInfo == null) {
			pnInfo = new JPanel();
			pnInfo.add(getLblPlayign());
			pnInfo.add(getLblSongName());
		}
		return pnInfo;
	}
	private JButton getBtnPlay() {
		if (btnPlay == null) {
			btnPlay = new JButton("▶");
			btnPlay.setEnabled(false);
			btnPlay.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					if(!getLsLocal().isSelectionEmpty() )
					{
						getBtnStop().setEnabled(true);
						getLblPlayign().setText("Playing: ");
						lblSongName.setText(getLsLocal().getSelectedValue().toString());
					}
					else
					{
						lblPlayign.setText("You must choose first the song from the local folder");
					}
				}
			});
		}
		return btnPlay;
	}
	private JButton getBtnStop() {
		if (btnStop == null) {
			btnStop = new JButton("◼");
			btnStop.setEnabled(false);
			btnStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) 
				{
					if( getBtnPlay().isEnabled())
					{
						getLblSongName().setText("");
						getLblPlayign().setText("");
					}
				}
			});
		}
		return btnStop;
	}
	
	
	public JLabel getLblDownloadProgress() {
		if (lblDownloadProgress == null) {
			lblDownloadProgress = new JLabel("");
		}
		return lblDownloadProgress;
	}
	public JProgressBar getProgressBar() {
		if (progressBar == null) {
			progressBar = new JProgressBar();
		}
		return progressBar;
	}

	public WorkersPool getWorkersPool() {
		// TODO Auto-generated method stub
		return workersPool;
	}
	
	

	private JLabel getLblPlayign() {
		if (lblPlayign == null) {
			lblPlayign = new JLabel("Choose a local path for the folder");
		}
		return lblPlayign;
	}
	private JLabel getLblSongName() {
		if (lblSongName == null) {
			lblSongName = new JLabel("");
		}
		return lblSongName;
	}
	
		
		
	public FileMonitor getFileMonitor()
	{
		return fileMonitor;
	}
		
	public String getPathForLocalFolder()
	{
		return PATH_LOCAL_FOLDER;
	}
		
	
		
	
	
		
	public DefaultListModel<String> getLocalListModel() {
		return localListModel;
	}
	public DefaultListModel<String> getSharedListModel() {
		return (DefaultListModel<String>) getLsShared().getModel();
	}
	
	public void setLocalListModel(DefaultListModel<String> localListModel) {
		this.localListModel = localListModel;
	}



	
	
	
	//------------------------------------------------------------------------------------------------------------------------
	//---------------------------Here ends the not very important section of code----------------------------------------------
	//--------------------------and start the important components and methods-----------------------------------------------
	//------------------------------------------------------------------------------------------------------------------------
	
	
	/**
	 * This method will create a button for downloading the files
	 * @return
	 */
	private JButton getBtnDownlad() {
		if (btnDownlad == null) {
			btnDownlad = new JButton("Downlad");
			btnDownlad.addActionListener(new ActionListener() {
				/**
				 * This action listrener work as follows:
				 * 	1) first check if there is a song selected
				 *  2) then it will update a list and create a worker (Multithreading) so the 
				 *  gui dont stop working
				 *  3) Will stop the timer for a little bit and execute the thread if it is not executing anything else
				 *  4) Done
				 */
				public void actionPerformed(ActionEvent e) 
				{
					if( !getLsShared().isSelectionEmpty())
					{
						String name = getLsShared().getSelectedValue().toString();
						getLocalListModel().addElement(name +"@ [downloading]");					
						DownloadUploadWorker downloadWorker = new DownloadUploadWorker(MAIN_WINDOW, name, getPathForLocalFolder(),"Shared_Folder", getLsLocal());
						timer.stop();
						workersPool.addWorker(downloadWorker);
						workersPool.executeNext();
						timer.start();
						getLblPlayign().setText("");
					}
					else
					{
						getBtnDownlad().setEnabled(false);
						getLblPlayign().setText("Select a file to download");
					}
				}

				
			});
			btnDownlad.setEnabled(false);
		}
		return btnDownlad;
	}
	
	
	
	
	/**
	 * This is the set up for the search local folder button
	 * @return the Jbutton
	 */
	private JButton getBtnSearchLocalFolder() {
	if (btnSearchLocalFolder == null) {
		btnSearchLocalFolder = new JButton("Search local folder");
		btnSearchLocalFolder.addActionListener(new ActionListener() {
			/**
			 * This listener creates a default JFileChosser in order to select the local folder directory
			 * It also stores the Local Folder path in the info file with the FileManager class and it also set up the folders monitors
			 */
			public void actionPerformed(ActionEvent e) 
			{
				String userhome = System.getProperty("user.home");
				JFileChooser folderChooser = new JFileChooser(userhome);
				folderChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				folderChooser.setDialogTitle("Folder chooser");
				folderChooser.showOpenDialog(rootPane);
				if( folderChooser.getSelectedFile() != null)
				{
					
					PATH_LOCAL_FOLDER =  folderChooser.getSelectedFile().getPath();
					localManager.setPathToLocal(PATH_LOCAL_FOLDER);
					fileManager.writeStringInFile("path", PATH_LOCAL_FOLDER);
					if(fileMonitor instanceof FileMonitorImp)
					{
						FileMonitorImp imp = (FileMonitorImp) fileMonitor;
						imp.setPathToLocalFolder(PATH_LOCAL_FOLDER);
					}
					getLsLocal().setEnabled(true);
					getBtnPlay().setEnabled(true);
					localObserver.update();
					getLblPlayign().setText("");
				}
				
			}
		});
	}
	return btnSearchLocalFolder;
}

	/**
	 * This is the set up for the upload button
	 * @return the Jbutton
	 */
	private JButton getBtnUpload() {
		if (btnUpload == null) {
			btnUpload = new JButton("Upload");
			btnUpload.addActionListener(new ActionListener() {
				
				/**
				 * This listener is very similar to the download and  it follows the same steps
				 */
				public void actionPerformed(ActionEvent e) 
				{
					if(!getLsLocal().isSelectionEmpty())
					{

						getLblPlayign().setText("");
						String name = getLsLocal().getSelectedValue().toString();
						getSharedListModel().addElement(name +"@ [uploading]");					
						DownloadUploadWorker uploadWorker = new DownloadUploadWorker(MAIN_WINDOW, name, "Shared_Folder", PATH_LOCAL_FOLDER, getLsShared());
						timer.stop();workersPool.addWorker(uploadWorker);
						workersPool.executeNext();
						timer.start();
					}
					else
					{
							getBtnUpload().setEnabled(false);
							getLblPlayign().setText("Select a file to upload");
						
					}
					
				}
			});
			btnUpload.setEnabled(false);
		}
		return btnUpload;
	}

	
	/**
	 * This method check if there is already store in the additional info file a path location
	 * for this computer and if not it will wait until the user click in the "Select local folder" button
	 */
	private void checkIfExistLocalFolder() {
		if(fileManager.isThereInfoFile())
		{
			PATH_LOCAL_FOLDER = fileManager.readData("path");
			if(PATH_LOCAL_FOLDER != null)
			{
				FileMonitorImp imp = (FileMonitorImp) fileMonitor;
				imp.setPathToLocalFolder(PATH_LOCAL_FOLDER);
				localManager.setPathToLocal(PATH_LOCAL_FOLDER);
				getLsLocal().setEnabled(true);
				getBtnPlay().setEnabled(true);
				localObserver.update();
				getLblPlayign().setText("");
			}
		
		}
			
		
	}
	
	
	


	
	

	
}
