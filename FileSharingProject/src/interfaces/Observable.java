package interfaces;

public abstract class Observable 
{
	
	public abstract void setChanged();
	public abstract boolean hasChanged();
	public  abstract void notifyObservers();
	public  abstract void addObserver(Observer o);

}
