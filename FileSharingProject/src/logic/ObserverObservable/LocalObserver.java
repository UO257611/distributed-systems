package logic.ObserverObservable;

import java.util.Observable;

import gui.MainWindow;
import interfaces.Observer;

/**
 * 
 * @author Eduardo Lamas Suárez
 * In this class is implemented the observer the Observer/Observable design pattern
 *
 */
public class LocalObserver  implements Observer
{
	private MainWindow mw;
	
	/**+
	 * Constructor 
	 * @param mw, main window of the gui
	 */
	public LocalObserver(MainWindow mw) {
		this.mw = mw;
	}
	
	/**
	 * This method will update the model of the local list
	 */
	@Override
	public void update() {
		mw.getLocalListModel().clear();
		for(String name : mw.getLocalManager().getNames())
			if(name != null)
				mw.getLocalListModel().addElement(name); 
		
		
	}
	
	

	
}
