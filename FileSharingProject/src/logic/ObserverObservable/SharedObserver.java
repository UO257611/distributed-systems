package logic.ObserverObservable;


import gui.MainWindow;
import interfaces.Observer;

/**
 * 
 * @author Eduardo Lamas Suárez
 * In this class is implemented the observer the Obverver/Observable design pattern
 *
 */
public class SharedObserver implements Observer{
	private MainWindow mw;
	
	/**
	 * Constructor
	 * @param mw, main window of the gui
	 */
	public SharedObserver(MainWindow mw) {
		this.mw = mw;
	}

	/**
	 * This method will update the shared model list
	 */
	@Override
	public void update() {
		mw.getSharedListModel().clear();;
		for(String name : mw.getFileMonitor().getNames())
				mw.getSharedListModel().addElement(name); 
		
		
	}

}
