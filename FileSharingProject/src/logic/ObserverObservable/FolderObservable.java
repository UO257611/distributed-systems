package logic.ObserverObservable;

import java.util.ArrayList;
import java.util.List;


import interfaces.FileMonitor;
import interfaces.Observable;
import interfaces.Observer;
import logic.Monitor.LocalFolderLogic;

/**
 * 
 * @author Eduardo Lamas Suárez
 * 
 * In this class is implemented the observable of the Observer/Observable design pattern
 *
 */
public class FolderObservable extends Observable
{
	private FileMonitor fm;
	private List<Observer> listObs;
	private LocalFolderLogic lf;
	
	/**
	 * Constructor
	 * @param fm
	 * @param lf
	 */
	public FolderObservable(FileMonitor fm, LocalFolderLogic lf) 
	{
		this.fm = fm;
		this.lf = lf;
		listObs = new ArrayList<Observer>();
	}
	

	/**
	 * Notify all observers if there has been a change in the folders
	 */
	@Override
	public synchronized void setChanged() 
	{
		if( hasChanged())
			notifyObservers();
		
	}

	/**
	 * Notify all the observers of a change
	 */
	@Override
	public void notifyObservers() {
		for(Observer x: listObs )
			x.update();
	}


	/**
	 * Check if in the folders has been changed
	 */
	@Override
	public synchronized boolean hasChanged() {
		return fm.isChange()|lf.isChange()?true:false;
	}


	/**
	 * Add an observer to the arraylist
	 */
	@Override
	public synchronized void addObserver(Observer o) {
		listObs.add(o);
	}
	

}
