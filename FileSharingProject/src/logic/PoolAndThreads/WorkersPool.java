package logic.PoolAndThreads;

import java.util.ArrayList;

import gui.MainWindow;

/**
 * @author Eduardo Lamas Suárez
 * In this class there are two design pattern implemented
 * 1º Singleton
 * 2º Thread Pool
 */
public class WorkersPool 
{
	private ArrayList<DownloadUploadWorker> pool;
	private static WorkersPool instance;
	private boolean inExecution;
	private MainWindow mw;

	
	/**
	 * Constructor of the class
	 * @param mw, main window of the gui
	 */
	private WorkersPool(MainWindow mw)
	{
		pool = new ArrayList<DownloadUploadWorker>();
		inExecution = false;
		this.mw = mw;
	}
	
	
	
	/**
	 * Notmal get
	 * @return
	 */
	public ArrayList<DownloadUploadWorker> getPool() {
		return pool;
	}


	/**
	 * Return the instance of the class in order to only have one instance in the program. If the Instance is not initialized
	 * this method will call the private constructor
	 * @param mw, main window of the gui
	 * @return an instance of this class
	 */
	public static WorkersPool getInstance(MainWindow mw)
	{
		if(instance == null)
			instance = new WorkersPool(mw);
	
		return instance;
	}
	
	/**
	 * Add a new worker to the pool
	 * @param worker, new worker (Thread)
	 */
	public void addWorker(DownloadUploadWorker worker)
	{
		instance.getPool().add(worker);
	}
	
	/**
	 * Check the amount of workers remaining
	 * @return int, the size of the pool
	 */
	public int checkAmountOfThread()
	{
		return instance.getPool().size();
	}
	
	/**
	 * When a worker finish, it will call this method, in order to delete his self of the pool
	 */
	public void finishedWorker()
	{
		instance.getPool().remove(0);
		inExecution = false;
		mw.getLblDownloadProgress().setText("Download done");
	}
	
	/**
	 * This method will reset the progress bar and call the next worker to execute if there is any 
	 * worker remaining
	 */
	public void executeNext()
	{
		if(!inExecution && checkAmountOfThread()>0)
		{	
			mw.getProgressBar().setValue(0);
			mw.getProgressBar().setMaximum(instance.getPool().get(0).getTotalBytes());
			mw.getProgressBar().repaint();
			mw.getLblDownloadProgress().setText("Downloading: "+instance.getPool().get(0).getName());
			inExecution = true;
			pool.get(0).execute();
			
		}
	}
	
	
}
