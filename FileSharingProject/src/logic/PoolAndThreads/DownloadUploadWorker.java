package logic.PoolAndThreads;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.SwingWorker;

import gui.MainWindow;
import interfaces.FileMonitor;


/**
 * 
 * @author Eduardo Lamas Suárez
 * In this class is implemented the workers with the  Swing api, in order
 * to use multithreating in the application so, the gui don't get freeze
 *
 * The worker created with this will be use in the WorkersPool object
 */
public class DownloadUploadWorker extends SwingWorker<byte[],Long>{
	private MainWindow mw;
	private String destionPath;
	private String sourcePath;
	private FileMonitor fileMonitor;
	private String name;
	private JList theModifiedList;
	private DefaultListModel<String> theModel;
	private WorkersPool pool;
	private int totalBytes;
	
	/**
	 * 
	 * @param mw, main window
	 * @param name, path
	 * @param destination, the folder where it is going to be storied
	 * @param source, of the file
	 * @param theModifiedList, jlist in which the song will appear
	 * Constructor
	 */
	public  DownloadUploadWorker(MainWindow mw, String name, String destination, String source, JList<String> theModifiedList) {
		super();
		this.theModifiedList = theModifiedList;
		this.sourcePath = source;
		this.destionPath = destination;
		this.mw = mw;
		fileMonitor = mw.getFileMonitor();
		this.name = name;
		if( theModifiedList.getModel() instanceof DefaultListModel)
			theModel = (DefaultListModel<String>)theModifiedList.getModel();
		totalBytes = getSizeOfFile(sourcePath + "/" +name);
		

		
	}
	
	/**
	 * This method is an alternative to the proposed in class for 
	 * knowing which is the size of the original file. This was done like this
	 * because the original proposition 
	 * @param string, name of the file
	 * @return int, size of the original file
	 */
	private int getSizeOfFile(String string) {
		byte[] x;
		int z = 0;
		try {
			x = Files.readAllBytes(new File(string).toPath());
			for (byte y : x)
				z+= Byte.toUnsignedInt(y);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return z;
	}
	
	
	
	/**
	 * Concurrency
	 */
	@Override
	protected synchronized byte[] doInBackground() {
		
		return downloadOrUploadFile();
	    
		
	  }
	
	
	/**
	 * Method that is executed when the doInBackground ends.
	 * Is only for updating the JLists in the gui, reset the JProggresbar
	 * and for calling the next thread (worker) in the pool
	 */
	@Override
	protected void done()
	{
		
		try {
			creatingFile(get());
		} catch (InterruptedException | ExecutionException e) {
			
			e.printStackTrace();
		}
		for(int i = 0; i < theModel.getSize(); i++)
		{
			if( theModel.getElementAt(i).equals(name + "@ [downloading]") || theModel.getElementAt(i).equals(name + "@ [uploading]"))
			{
				theModel.removeElementAt(i);
				theModel.addElement(name);
				break;
			}
		}
		mw.getProgressBar().setValue(0);
		mw.getProgressBar().repaint();
		mw.getWorkersPool().finishedWorker();
		mw.getWorkersPool().executeNext();
		
	}
	/**
	 * 
	 * @param fileAsArray, array with the bytes of the song
	 * 
	 * this method will reconstruct the file from a array of byte
	 */
	private synchronized void creatingFile(byte[] fileAsArray) {
		File finishedFile = new File(destionPath+"/"+name);
		
		try 
		{
			FileOutputStream ouputStream = new FileOutputStream(finishedFile);
			ouputStream.write(fileAsArray);
			ouputStream.close();
		}catch(IOException x)
		{
			x.printStackTrace();
		}
	}
	
	/**
	 * This method will download or upload the file to the folder
	 * as an array of bytes
	 * 
	 * @return array of bytes of the file
	 */
	public synchronized byte[] downloadOrUploadFile()
	{
		byte tempByte;
		ArrayList<Byte> newFile = new ArrayList<Byte>();
		fileMonitor.openFile(sourcePath+"/"+name);
		tempByte = fileMonitor.getB();

		while(!fileMonitor.isEOF())
		{
			newFile.add(tempByte);
			tempByte = fileMonitor.getB();
			publish(Byte.toUnsignedLong(tempByte));
		}
		
		fileMonitor.closeFile(name);
		
		byte[] fileAsArray = new byte[newFile.size()];
		
		for(int i = 0; i< newFile.size();i++)
			fileAsArray[i] = newFile.get(i);
		
		return fileAsArray;
	}
	
	/**
	 * Method that return the file name of the downloaded/uploaded file
	 * @return string name of file Downloaded/Uploaded
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Return the dowload or uploaded original finel size
	 * @return int the total amount of bytes
	 */
	public int getTotalBytes()
	{
		return totalBytes;
	}
	
	/**
	 * Method that is call in the downloadOrUpload with publish method, and is 
	 * used to calculate how much of the file was downloaded
	 * and update the JProgressbar in the gui
	 */
	@Override
	protected void process(List<Long> chunks) {

		int i = 0;
		for(long x: chunks)
			i += x;

		mw.getProgressBar().setValue(mw.getProgressBar().getValue()+i);
		mw.getProgressBar().repaint();
	}
	
	
	
	
	
	

}
