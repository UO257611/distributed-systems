package logic.Monitor;

import java.util.ArrayList;
import interfaces.FileMonitor;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 
 * @author Eduardo Lamas Suárez
 * In this class it is implemented the FileMonitor
 * 
 * This class use the Singleton Pattern
 */

public class FileMonitorImp implements FileMonitor 
{
	
	private final static Byte ERROR_WHILE_READING_BYTES = -1;
	private final static Byte EOF_REACHED = -2;
	private boolean finished;
	private File myOpenFile;
	private DataInputStream dataInputStream;
	private String PATH_TO_SHARED = "Shared_Folder";
	private String PATH_TO_LOCAL = "";
	private String[] names;
	private static FileMonitorImp instance;
	
	/**
	 * Constructor
	 */
	private  FileMonitorImp() {
		finished = true;
	}
	
	
	/**
	 * Return the instance of the class in order to only have one instance in the program. If the Instance is not initialized
	 * this method will call the private constructor
	 * @return an instance of this class
	 */
	public static FileMonitorImp getInstance()
	{
		if(instance == null)
		{
			instance = new FileMonitorImp();
		}
		return instance;
	}


	/**
	 * Check if it the end of file 
	 */
	@Override
	public boolean isEOF() {
		return instance.finished;
	}
	
	/**
	 * Set the path to the local folder
	 * @param path, to the local folder
	 */
	public void setPathToLocalFolder(String path)
	{
		instance.PATH_TO_LOCAL = path;
	}

	/**
	 * Get the names of the shared folder files that are mp3 and wav format
	 */
	@Override
	public synchronized String[] getNames() {
		File dir = new File(instance.PATH_TO_SHARED);
		File[] files = dir.listFiles();
		instance.names = new String[files.length];
		for(int i = 0; i< files.length; i++)
			if(files[i].getName().toLowerCase().endsWith(".mp3")||files[i].getName().toLowerCase().endsWith(".wav"))
				instance.names[i] = files[i].getName();

		return names;
	}
	


	/**
	 * This method will open the new file, that is going to be downloaded
	 */
	@Override
	public synchronized boolean openFile(String name)  {
		instance.finished =false;
		myOpenFile = new File(name);

		try {
			dataInputStream = new DataInputStream(new FileInputStream(myOpenFile));
	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}   

		return true;
	}

	/**
	 * This method get a byte from the original file
	 */
	@Override
	public synchronized byte getB()  {
		try {
			int myInt = dataInputStream.read();
			if(myInt<0)
			{
				instance.finished =true;
				return EOF_REACHED;
			}
			else
				return (byte)myInt;
		} catch (IOException e) {
			e.printStackTrace();
			return ERROR_WHILE_READING_BYTES;
			
		}
		
	}

	/**
	 * This will close the new file
	 */
	@Override
	public synchronized boolean closeFile(String name) {
		try {
			dataInputStream.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * this will check if there was any change in the shared folder
	 */
	@Override
	public synchronized boolean isChange() {
		String[] copy = instance.names.clone();
		String[] newCheck = getNames();
		return !copy.equals(newCheck);
	}

}
