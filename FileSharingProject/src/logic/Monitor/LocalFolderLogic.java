package logic.Monitor;

import java.io.File;

/**
 * 
 * @author Eduardo Lamas Suárez
 * Another version of file monitor
 *
 * This class use the Singleton Pattern
 *
 */
public class LocalFolderLogic 
{
	String[] names = new String[1];
	private static LocalFolderLogic instance;
	private static String PATH_TO_LOCAL;
	
	/**
	 * Constructor
	 */
	private LocalFolderLogic()
	{		
	}
	
	/**
	 * Return the instance of the class in order to only have one instance in the program. If the Instance is not initialized
	 * this method will call the private constructor
	 * @return an instance of this class
	 */
	public static LocalFolderLogic getInstance()
	{
		if(instance == null)
			instance = new LocalFolderLogic();
		return instance;
		
	}
	
	/**
	 * Set the path to the local folder
	 * @param path, to local folder
	 */
	public void setPathToLocal(String path)
	{
		PATH_TO_LOCAL = path;
	}
	
	
	/**
	 * It gets the names of the local folder file
	 * @param pathToLocal
	 * @return
	 */
	public String[] getNames()
	{
		File dir = new File(PATH_TO_LOCAL);
		File[] files = dir.listFiles();
		instance.names = new String[files.length];
		for(int i = 0; i< files.length; i++)
			if(files[i].getName().toLowerCase().endsWith(".mp3")||files[i].getName().toLowerCase().endsWith(".wav"))
				instance.names[i] = files[i].getName();

		return instance.names;
	}
	/**
	 * Checks if the folder has been modified
	 * 
	 * @param pathToLocal, to the folder
	 * @return boolean true, if it was, false if not.
	 */
	public boolean isChange() {
		String[] copy = instance.names.clone();
		String[] newCheck = getNames();
		return !copy.equals(newCheck);
	}

}
