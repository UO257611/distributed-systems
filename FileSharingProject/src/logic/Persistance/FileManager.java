package logic.Persistance;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 
 * @author Eduardo Lamas Suárez
 * The aim of this class is the one of writing in a file and reading from in it.
 * The way of reading/writing similar to the dictionaries
 *
 */
public class FileManager 
{
	private final String FOLDER_NAME = "AditionalFiles";
	private final String FILE_NAME = "additionalInfo.txt";
	private final String COMPUTER_NAME ;
	private final int TOTAL_FILES = 2;
	
	/**
	 * Constructor, it gets the computerName
	 */
	public FileManager()
	{
		COMPUTER_NAME = getComputerName();
	}
	
	/**
	 * Check if there is info File
	 * @return
	 */
	public boolean  isThereInfoFile()
	{
		
		File folder = new File(FOLDER_NAME);
		File[] files = folder.listFiles();
		
		return files.length < TOTAL_FILES ? false: true;
		
	}
	
	/**
	 * Write a string to the info file, each string has associated a keyword in order to find it
	 * @param keyWord, string use to search some data
	 * @param words, string with the data
	 */
	public void writeStringInFile(String keyWord, String words)
	{
		File newInfoFile;
		try {
			
			
			
			if(!isThereInfoFile())
			{
				newInfoFile = new File(FOLDER_NAME +"/"+ FILE_NAME );
				words = COMPUTER_NAME + "@" +keyWord+"@"+words+"@\n";
				}
			else
			{
				words = fileToStringAndUpdate(keyWord, words);
			}
			BufferedWriter writer = new BufferedWriter(new FileWriter(FOLDER_NAME +"/"+ FILE_NAME ));
			writer.write(words);
			writer.close();
			
		} catch (Exception e)
		{
			System.out.println("A problem has occur while trying to write the file");
			e.printStackTrace();
		}
	}
	
	/**
	 * This method read the data associated to a keyword
	 * @param keyword, string used to search the data in the info file
	 * @return string with the data if it is found, null if there is no data find. I know that this is not a very good
	 * practice 
	 */
	public String readData(String keyword)
	{
		String word = null;
		try
		{
			BufferedReader reader = new BufferedReader(new FileReader(FOLDER_NAME +"/"+ FILE_NAME ));
			while(reader.ready())
			{
				String line = reader.readLine();
				String[] splittedLine = line.split("@");
				if(COMPUTER_NAME.equals(splittedLine[0]) && splittedLine[1].equals(keyword))
				{
					word = splittedLine[2];
					break;
				}
			}
			reader.close();
		}catch (Exception e)
		{
			System.out.println("A problem has occur while trying to read the file");
			e.printStackTrace();
		}
		return word;
	}
	
	
	/**
	 * In this method is where all the files updates are made, first reads and copy to a stringbuilder the data of the file, and if there
	 * is a computer, with a keyword and data already stored, and the program needs to update that data, it is done on the first loop
	 * If the keyword or the computer name is not found in the first loop, it is added at the end of the file.
	 * This way of persistence is not very efficient. 
	 * @param keyword, string associated to a data
	 * @param words, string data
	 * @return string with the content updated of all file
	 * @throws Exception if there is and i/o exception
	 */
	private String fileToStringAndUpdate(String keyword , String words) throws Exception
	{
			boolean found = false;
			StringBuilder  fileString = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(FOLDER_NAME +"/"+ FILE_NAME ));
			while(reader.ready())
			{
				String line = reader.readLine();
				String[] splittedLine = line.split("@");
				if(splittedLine[0].equals(COMPUTER_NAME) && splittedLine[1].equals(keyword))
				{
					found = true;
					fileString.append(COMPUTER_NAME+ "@" + keyword+"@"+words+ "\n");
				}
				else
				{
					fileString.append(line + "\n");
				}
			}
			if( !found)
				fileString.append(COMPUTER_NAME+ "@" + keyword+"@"+words+ "\n");
			
			reader.close();
			return fileString.toString();
		
	}
	
	/**
	 * Method used to  get the computer name
	 * @return a string with the computer name, if it is not founded it will write Unknown
	 */
	public String getComputerName()
	{
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			
			e.printStackTrace();			
		}
		return "UNKNOWN";
	}
}
