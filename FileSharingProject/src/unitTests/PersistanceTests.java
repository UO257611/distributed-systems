package unitTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import logic.Persistance.FileManager;

class PersistanceTests {

	@Test
	void testGetComputerNamer() {
		String myName = "Skynet";
		FileManager manager = new FileManager();
		String obtainedname = manager.getComputerName();
		assertEquals(myName, obtainedname);
	}

}
