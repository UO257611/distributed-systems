package logic.server.temphum;

import java.net.MalformedURLException;
import  java.rmi.*;
import java.rmi.server.*;

public class TempHumServer  extends UnicastRemoteObject implements TempHumInterface
{
	private static final String SERVER_NAME ="TEMPHUM";
	private TempHumMeasure measures;
	
	/**
	 * Constructor for the temperature/humidity server3245
	 * I only creates a thread for generating random values
	 * @throws RemoteException
	 */
	public  TempHumServer() throws RemoteException {
	
			measures = new TempHumMeasure();
			measures.start();
		
	}
	
	
	/**
	 * In this method the server is bind to the rmiregistry
	 * @param args
	 */
	public static void main(String[] args)
	{
		try {
			TempHumServer server = new TempHumServer();
			Naming.rebind(SERVER_NAME, server);
		}
		catch (RemoteException e) {
			System.out.println("Communication error " + e.toString());

		}
		catch (MalformedURLException e) {
			System.out.println("Malformed URL for MessageServer name " + e.toString());
		}
		
	}
	
	/***
	 * It return the value of the thread cover value generator, that represents the value of the 
	 * temperature sensor
	 */
	@Override
	public int getTemperature() throws RemoteException {
		return measures.getTemperature();
	}
	
	/***
	 * It return the value of the thread cover value generator, that represents the value of the 
	 * humidity sensor
	 */
	@Override
	public int getHumidity() throws RemoteException {
		return measures.getHumidity();
	}

}
