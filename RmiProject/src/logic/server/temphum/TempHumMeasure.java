package logic.server.temphum;

import java.util.Random;

public class TempHumMeasure extends Thread
{
	private Random random;
	private int temperature;
	private int humidity;
	
	public TempHumMeasure()
	{
		random = new Random();
	}
	
	/**
	 * This method will generate numbers between 1 and 99 for the humidity and temperature
	 */
	public void run()
	{
		while(true)
		{
			try
			{
			temperature = random.nextInt(99)+1;
			humidity = random.nextInt(99)+1;
			System.out.println("Temperature: "+ temperature+"\n Humidity: "+humidity);
			Thread.sleep(1500);
			}catch(Exception e)
			{
				System.out.println("Error while taking measures of temperature and humidity");
				e.printStackTrace();
			}
			
			
		}
	}

	/**
	 * This method will return the generated measure of the temperature
	 * @return the temperature value
	 */
	public int getTemperature() {
		return temperature;
	}

	/**
	 * This method will return the generated measure of the humidity
	 * @return the huumidity
	 */
	public int getHumidity() {
		return humidity;
	}

}
