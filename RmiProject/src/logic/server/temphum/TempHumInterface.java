package logic.server.temphum;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TempHumInterface extends Remote
{
	int getTemperature() throws RemoteException;
	int getHumidity() throws RemoteException;

}
