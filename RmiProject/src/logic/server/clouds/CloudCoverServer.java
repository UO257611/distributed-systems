package logic.server.clouds;
import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.server.*;
import java.lang.*;
import java.util.*;

public class CloudCoverServer extends UnicastRemoteObject implements CloudInterface
{
	
	private static final String SERVER_NAME = "CLOUD";
	private CloudMeasure cloudCoverage;
	
	/**
	 * Costructor for the cloud cover server
	 * I only creates a thread for generating random values
	 * @throws RemoteException
	 */
	public  CloudCoverServer() throws RemoteException {
		cloudCoverage = new CloudMeasure();
		cloudCoverage.start();
	}
	
	
	/***
	 * It return the value of the thread cover value generator, that represents the value of the 
	 * cloud cover sensor
	 */
	@Override
	public int getCloudCoverage() throws RemoteException {
		return cloudCoverage.getMeasure();
	}
	
	
	/**
	 * In this method the server is bind to the rmiregistry
	 * @param args
	 */
	public static void main(String[] args) 
	{
		CloudCoverServer cloudServer;
		try {

			cloudServer = new CloudCoverServer();
			Naming.rebind(SERVER_NAME, cloudServer);
			
		} 
		catch (RemoteException e) {
			System.out.println("Communication error " + e.toString());

		}
		catch (MalformedURLException e) {
			System.out.println("Malformed URL for MessageServer name " + e.toString());
		}
	}
	
}
