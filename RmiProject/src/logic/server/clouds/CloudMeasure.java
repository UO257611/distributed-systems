package logic.server.clouds;

import java.util.Random;

public class CloudMeasure  extends Thread{
	private int measure;
	private Random random = new Random();
	
	/**
	 * In this method, values between 0 y 99
	 */
	public void run()
	{
		while ( true)
		{
			try {
			measure = random.nextInt(99+1);
			System.out.println(measure);
			Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * This method will return the generated measures
	 * @return the cloud cover value
	 */
	public int getMeasure() {
		return measure;
	}

}
