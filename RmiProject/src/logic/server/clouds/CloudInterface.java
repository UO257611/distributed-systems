package logic.server.clouds;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CloudInterface extends Remote 
{
	public int getCloudCoverage() throws RemoteException;
}
