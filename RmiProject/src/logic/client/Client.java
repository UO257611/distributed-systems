package logic.client;
import java.net.MalformedURLException;
import java.rmi.*;

import logic.server.clouds.CloudCoverServer;
import logic.server.clouds.CloudInterface;
import logic.server.temphum.TempHumInterface;

public class Client 
{
	private static final String CLOUD_SERVER ="CLOUD";	
	private static final String TEMPHUM_SERVER ="TEMPHUM";
	private CloudInterface cloudSensor;
	private TempHumInterface tempHumSensors;
	
	/**
	 * The constructor of the client, where the client searchs for the 
	 * two rmi servers for the temperature/humidity and the cloud cover server
	 */
	public Client(){
		
		
		Remote cloudRemoteObject;
		Remote tempHumRemoteObject;
		try {
			
			cloudRemoteObject = Naming.lookup(CLOUD_SERVER);
			tempHumRemoteObject = Naming.lookup(TEMPHUM_SERVER);
			
			cloudSensor = (CloudInterface)cloudRemoteObject ;
			tempHumSensors=(TempHumInterface) tempHumRemoteObject;
			
			
		} catch (MalformedURLException e) {
			System.out.println("Malformed url");
			e.printStackTrace();
		} catch (RemoteException e) {

			System.out.println("Remoted exeception");
			e.printStackTrace();
		} catch (NotBoundException e) {

			System.out.println("Not Bound");
			e.printStackTrace();
		}
	}
	
	/**
	 * In this method is where all the magic happens.
	 * The client gets the sensor value for all the severs and calculate an alert value
	 * Then if the alert value is too high, an alert will be send, if not, a message will be sent
	 * telling the user, that there is no risk found
	 * @param args
	 */
	public static void main(String[] args)
	{
		Client client = new Client();
		while(true)
		{
			try {
				Thread.sleep(10000);
				
				// Getting the sensor values
				int cloud = client.cloudSensor.getCloudCoverage();
				int temp = client.tempHumSensors.getTemperature();
				int hum= client.tempHumSensors.getHumidity();
				//Some debugging messages
				//System.out.println("Cloud cover percentage: " + cloud);
				//System.out.println("Temperature percentage: "+ temp);
				//System.out.println("Humidity percentage: " +hum);
				
				//Calculating the alert value
				int alert =  (temp) * (1 - (hum/100))*(1- (cloud/100));
				//Selector
				if	(alert > 20)
					System.out.println("Checking.. risk detected (" + alert+")");
				else 
					System.out.println("Checking.. No risk");
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
