package logic.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import logic.extra.Question;


/**
 * This is the general client class
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public class Client  implements Runnable
{
	private final int PORT = 8000;
	private final String END_CONECTION ="5 ";
	private final String REGISTER ="1 ";
	private final String QUESTION ="2 ";
	private final String ANSWER_REQUEST ="3 ";
	private final String SCORE ="4 ";
	private final String HOST ="localhost";
	private final int MENU = 0;
	private final int ANSWER = 1;
	private final int RESULT = 2;
	private final String FINISH = "NOT_MORE";
	
	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private boolean nextQuestion;
	private Scanner console;
	private String username;
	private int selctorOfAction;

	
	/**
	 * Constructor of the client class.
	 */
	public Client()
	{
		nextQuestion = true;
		selctorOfAction =0;
		console = new Scanner(System.in);
	}
	
	/**
	 * The main method of the class, it will set up  the socket logic 
	 * and start working
	 * @param args
	 */
	public static void main(String[] args)
	{
		Client client = new Client();
		try {
			client.setUp();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This method will set up the socket and it will start the quiz
	 * This method will analyze the code provided by the server, paint the menu and send the request
	 * @throws IOException
	 */
	public void setUp() throws IOException 
	{
		//Socket set up process
			socket = new Socket(HOST, PORT);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			
			System.out.println("Connected to Server");	
			//Register a new user
			register();
			
			//This will execute while there is more questions
			while(nextQuestion)
			{
				//Selector of actions
				switch(selctorOfAction)
				{
				
					case MENU:
						//paint the menu
						paintMenu();
						//Next logic action
						selctorOfAction++;
						break;
				
					case ANSWER:
						try {
							//This next line will clean the console
							System.out.print("\033[H\033[2J"); 
							//Reply of the server with the question
							String questionString = (String)in.readObject();
							if(!questionString.contains(FINISH))
								//process for answering th question
								answer(questionString);
							else
							{
								// There is no more qeustion
								System.out.println("There is no reamining question");
								out.writeObject(SCORE + username);
								nextQuestion = false;
							}
						} catch (ClassNotFoundException e)
						{
							System.out.println("CONNECTION ERROR: Recieving the question");
							e.printStackTrace();
						}
						// Next logic action
						selctorOfAction++;
						break;
					case RESULT:
						try {
							//This next line will clean the console
							System.out.print("\033[H\033[2J"); 
							//Result replied by the server
							System.out.println((String) in.readObject());
						} catch (ClassNotFoundException e) {
							System.out.println("CONNECTION ERROR: Recieving the result");
							e.printStackTrace();
						}
						//Restart the processs
						selctorOfAction = MENU;
						break;
						
					}
				
				

				}
			// End connection 
			out.writeObject(END_CONECTION + username);
			
			
	}

	/**
	 * This method will parse the question, analyze the reply of the user,
	 * and send it to the server
	 * @param questionString
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void answer(String questionString) throws IOException, ClassNotFoundException {
		//This will parse the question and print it on the question
		printQuestion(parseQuestion(questionString));
		System.out.println("Write the number of the answer?");
		//This will read the command line answer and check if it is valid
		// It also sends the answer to the server if it is correct
		String option = console.nextLine();
		if(option.equals("1")||option.equals("2") || option.equals("3") || option.equals("4"))
		{
			String[] fixedoption = option.split("\n"); 
			out.writeObject(ANSWER_REQUEST + fixedoption[0]+ " " + username);
		}
		else 
		{
			// If the answer is not 1,2 or 3 the question will be asked again
			System.out.println("Write only the number from 1 to 4");
			answer(questionString);
		}
	}

	/**
	 * This method will print the menu in the screen and 
	 * send the option choose by the user
	 * @throws IOException
	 */
	private void paintMenu() throws IOException {
		//Print the menu
		System.out.println("Menu:");
		System.out.println("\t Press 'a' for a question");
		System.out.println("\t Press 'b' for see the puntuation");
		System.out.println("\t Press 'c' for closing the questionary");
		// Read the user command and analyze it
		String comand = console.nextLine();
		if(comand.endsWith("a") || comand.endsWith("b"))
		{
			// Send the command to the sercer
			comand = analyseMenuCommand(comand);
			out.writeObject(comand);
		}
		else if(comand.endsWith("c"))
		{
			//Exit the quiz
			nextQuestion = false;
		}
		else
		{
			//Invalid option
			System.out.println("Try again,  but only write 'a' or 'b'");
			paintMenu();
		}
		
		if(comand.startsWith(SCORE))
		{
			try {
				//This next line will clean the console
				System.out.print("\033[H\033[2J"); 
				System.out.println((String) in.readObject());
				paintMenu();
			} catch (ClassNotFoundException e) {
				System.out.println("CONNECTION ERROR: Recieving the result");
				e.printStackTrace();
			}
		}
	}
	
	// This will parse the question
	private Question parseQuestion(String givenString)
	{
		String[] splittedline = givenString.split(";");
		return new Question(splittedline[0], splittedline[1], splittedline[2], splittedline[3], splittedline[4], Integer.parseInt(splittedline[5]));

	}
	
	//This will print the question with style on the screen
	private void printQuestion(Question question) 
	{
		System.out.println(question.getQuestion());
		System.out.println("\t " + question.getAnswer1());
		System.out.println("\t " + question.getAnswer2());
		System.out.println("\t " + question.getAnswer3());
		System.out.println("\t " + question.getAnswer4());
	}

	/**
	 * This method will analyze the user command on the menu 
	 * @param command written by the user
	 * @return string with the message that is going to be send it to the server 
	 */
	private String analyseMenuCommand(String command) {
		String message = "";
		switch(command)
		{
			case("a"):
				message = QUESTION + username;
				break;
			case("b"):
				message = SCORE + username;
				break;
			case("c"):
				message = END_CONECTION + username;
				break;
			default:
				System.out.println("You just write a not possible option");
		}
		return message;
		
	}

	private void register() throws IOException
	{
		System.out.println("What is your name?");
		String name = console.nextLine();
		username = name.split("\n")[0];
		String command = REGISTER + username;
		out.writeObject(command);
	}

	@Override
	public void run() {
		try {
			while(nextQuestion)
			{
				
					setUp();
				} 
			}catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	}
	

	
	

}
