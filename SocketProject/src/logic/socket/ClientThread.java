package logic.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import logic.extra.Question;
import logic.extra.User;

/**
 * This class is a thread that will be created on the server side
 * in order to be able to manage several clients at the same time.
 * One thread per class
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public class ClientThread implements Runnable {

	private static final int TOTAL_NUMBER_OF_QUESTIONS = 9;
	private ObjectInputStream in ;
	private ObjectOutputStream out;
	private HashMap<String, User> users;
	private ArrayList<Question> questions;
	private Socket clientSocket;
	public boolean isFinished;

	/**
	 * This is the creator of the client thread class used for multi-client purpose
	 * @param clientSocket, the specific socket of the client
	 * @param users, dictionary of the user names/user object
	 * @param questions, Arraylist of question
	 * @throws IOException
	 */
	public ClientThread(Socket clientSocket,  HashMap<String, User> users,
			ArrayList<Question> questions) throws IOException {
		this.clientSocket = clientSocket;
		this.users = users;
		this.questions = questions;
		isFinished = false;
		
		
	}


	/**
	 * This method will analyze the command received from the user client and act depending of the
	 * user command
	 * @param userCommand, string with the user request
	 */
	private void analyzeCommand(String userCommand) {
		
		String[] splittedCommand = userCommand.split(" ");
		switch(splittedCommand[0])
		{
		//Register if the user is new
			case "1":
				if(!users.containsKey(splittedCommand[1]))
					users.put(splittedCommand[1], new User(splittedCommand[1]));
				System.out.println("The user " + splittedCommand[1] + " was added to the server");
				break;
			//If there is a question request
			case "2":
				System.out.println("Receiving Question Request");
				sendQuestion(splittedCommand);
				break;
			//Request for checking an answer
			case "3":
				System.out.println("Recibing Answer Request");
				checkAnswer(splittedCommand);
				break;
			//Request for quiz state
			case "4":
				sendActualQuestionaryState(splittedCommand);
			break;
			//Close the connection without crashing
			case "5":
				closeConnection();
			break;
				
		}
		
	}


	/**
	 * This will close the connection without crashing
	 */
	private void closeConnection() {
		try {
			in.close();
			out.close();
			clientSocket.close();
			isFinished = true;
		}catch(Exception e)
		{
			System.out.println("ERROR WHILE CLOSING THE THREAD");
		}
	}

	/**
	 * Send the quiz state of the user
	 * @param splittedCommand, command sent by the client
	 */
	private void sendActualQuestionaryState(String[] splittedCommand) {
		try {
			System.out.println("Recibing Final Game State Request");
			out.writeObject("FINAL SCORE: " + users.get(splittedCommand[1]).getScore() + " - " + "Questions done: " + (users.get(splittedCommand[1]).getLastQuestion() +1) + "/ 10");
		} catch (IOException e) {
			System.out.println("CONNECTION ERROR: Sending Final Game State");
			e.printStackTrace();
		}
	}


	/**
	 * Check if the answer was correct, if it was the score is updated
	 * @param splittedCommand, command sent by the client
	 */
	private void checkAnswer(String[] splittedCommand) 
	{
		if(questions.get(users.get(splittedCommand[2]).getLastQuestion()).getCorrectAnswer() == Integer.parseInt(splittedCommand[1]))
					users.get(splittedCommand[2]).addScore();
		try {
				out.writeObject("Your score: " + users.get(splittedCommand[2]).getScore() + " - " + "Questions done: " + (users.get(splittedCommand[2]).getLastQuestion()+1) + "/ 10");
		} catch (IOException e) {
				System.out.println("CONNECTION ERROR: Sending answer");
				e.printStackTrace();
		}
	}


	/**
	 * This send the next question
	 * @param splittedCommand, command sent by the client
	 */
	private void sendQuestion(String[] splittedCommand) {
		users.get(splittedCommand[1]).addLastQuestion();
		System.out.println(users.get(splittedCommand[1]).getLastQuestion());
		if(users.get(splittedCommand[1]).getLastQuestion() < TOTAL_NUMBER_OF_QUESTIONS)
		{
			Question question = questions.get(users.get(splittedCommand[1]).getLastQuestion());
			try 
			{
				out.writeObject(question.toString());
				
			} catch (IOException e) 
			{
				System.out.println("CONNECTION ERROR: Sending question");
				e.printStackTrace();
			}
		}
		else
		{
			try {
				out.writeObject("NOT_MORE");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * For being able to create a thread with this class, initialize some
	 * fields
	 */
	public void run()
	{
		try {
			out = new ObjectOutputStream(clientSocket.getOutputStream());
			in = new ObjectInputStream(clientSocket.getInputStream());
			
		while(!isFinished)
		{
			
			String userCommand = (String) in.readObject();	
			System.out.println(userCommand);
			analyzeCommand(userCommand);		
		

		}
		
		}
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	}
	
	
	
	
	
	
}
