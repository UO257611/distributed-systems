package logic.socket;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.SplittableRandom;

import logic.extra.FileManager;
import logic.extra.Question;
import logic.extra.User;

/**
 * This is the general server class
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public class Server 
{

	private final int PORT = 8000;
	private ServerSocket server;
	private Socket socket;
	private HashMap<String, User> users = new HashMap<String, User>() ;
	private ArrayList<Question> questions;
	private ArrayList<Thread> clients = new ArrayList<>();
	
	/**
	 * This is a simple main class
	 * @param args
	 */
	public static void main(String[] args)
	{
		try
		{
		Server server = new Server();
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 *This is the constructor of thec class
	* First of all, the questions are read and loaded into the server
	* At the end the server set up the server socket and it starts listening for
	* clients
	*/
	public Server() throws  IOException, ClassNotFoundException
	{
		setUpQuestions();
		setUpSockets();
	}
	
	/**
	 * Set up the server socket and it keeps listening for clients.
	 * If a client is detected, a new client thread will be created
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void setUpSockets() throws IOException, ClassNotFoundException
	{

		server = new ServerSocket(PORT);
		while(true)
		{
		ClientThread newClient = new ClientThread(server.accept(), users, questions);
		new Thread(newClient).start();
		System.out.println("New client");
		}
		
	}
	
	/**
	 * This will set up the Questions
	 */
	public void setUpQuestions()
	{
		questions = FileManager.readQuestions("questionList.txt");
		
	}
	
	
	
	
	
	
}
