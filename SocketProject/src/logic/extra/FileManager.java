package logic.extra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;


/**
 * This is the clas used to read from a file
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public  class FileManager 
{
	
	/**
	 * This method reads the question from a file
	 * @param fileName, the name of the file
	 * @return an arraylist with the question objects
	 */
	public static ArrayList<Question> readQuestions(String fileName)
	{
		ArrayList<Question> questions = new ArrayList<>();
		String line;
		try 
		{
			
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			while(reader.ready())
			{
				line = reader.readLine();
				questions.add(createQuestion(line));
				
			}
		
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return questions;

		
	}

	/**
	 * This method takes a string, a line from a text file, and with that
	 * this method will create a Question object
	 * @param line, string from questionList.txt
	 * @return a question object
	 */
	private static Question createQuestion(String line) {
		String[] splittedline = line.split(";");
		Question question = new Question(splittedline[0],splittedline[1],splittedline[2],splittedline[3],
				splittedline[4], Integer.parseInt(splittedline[5]));
		return question;
	}

}
