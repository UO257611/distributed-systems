package logic.extra;

import java.io.Serializable;




/**
 * This is class is only for managing easily the question
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public class Question  
{
	
	private String question;
	private String answer1;
	private String answer2;
	private String answer3;
	private String answer4;
	private int correctAnswer;
	
	/**
	 * Constructor of the question object
	 * @param question, a string with the question
	 * @param answer1, a string with the option 1
	 * @param answer2, a string with the option 2
	 * @param answer3, a string with the option 3
	 * @param answer4, a string with the option 4
	 * @param correctAnswer,, a string with the correct answer
	 */
	public Question(String question, String answer1, String answer2, String answer3, String answer4, int correctAnswer)
	{
		this.question = question;
		this.answer1 = answer1;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
		this.correctAnswer = correctAnswer;
	}
	
	/**
	 * This method return the question.
	 * @return a string with the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * This method return a question object string representation 
	 */
	@Override
	public String toString()
	{
		return question + ";1. " + answer1 + ";2. " + answer2 + ";3. " + answer3 + ";4. " + answer4+";" +correctAnswer;
	}
	
	/**
	 * This method return the option 1.
	 * @return a string with the option 1
	 */
	public String getAnswer1() {
		return answer1;
	}

	/**
	 * This method return the option 2.
	 * @return a string with the option 2
	 */
	public String getAnswer2() {
		return answer2;
	}

	/**
	 * This method return the option 3.
	 * @return a string with the option 3
	 */
	public String getAnswer3() {
		return answer3;
	}

	/**
	 * This method return the option 4.
	 * @return a string with the option 4
	 */
	public String getAnswer4() {
		return answer4;
	}

	/**
	 * This method return the correct answer.
	 * @return a string with the correct answer
	 */
	public int getCorrectAnswer()
	{
		return correctAnswer;
	}
	

}
