package logic.extra;


/**
 * This is class is for managin the users quiz state (points, question answer,...)
 * @author Eduardo Lamas Suárez - R00171724
 *
 */
public class User {
	
	private final int NOT_STARTED = -1;
	private String name;
	private int lastQuestion;
	private int score;
	
	/**
	 * Constructor for the user object
	 * @param name, string with the name of the user
	 */
	public User(String name)
	{
		this.name = name;
		lastQuestion = NOT_STARTED;
		score = 0;
	}
	
	/**
	 * Add 10 points to the user score
	 */
	public void addScore()
	{
		score+=10;
	}
	
	/**
	 * Increse the question counter
	 */
	public void addLastQuestion()
	{
		lastQuestion++;
	}

	/**
	 * A method that return the name of the user
	 * @return string with the user name
	 */
	public String getName() {
		return name;
	}

	/**
	 * A method that return the number of the last qestion  
	 * @return int of the question counter
	 */
	public int getLastQuestion() {
		return lastQuestion;
	}

	/**
	 * This method return the score of the user
	 * @return int with the score
	 */
	public int getScore() {
		return score;
	}
	

}
